<?php
function csvgenrateAction(){
    try{
    //$filter_data = json_decode('{"selectallapps":"0","app_name":["Facebook"],"platform":["Android","IPhone"],"country":["Afghanistan","Aland Islands","Albania","Algeria","American Samoa","Andorra","Angola","Anguilla","Antarctica","Antigua and Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","BoliviaBolivia","Bosnia and Herzegovina","Botswana","Bouvet Island","Brazil","British Indian Ocean Territory","Brunei Darussalam","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central African Republic","Chad","Chile","China","Christmas Island","Cocos (Keeling) Islands","Colombia","Comoros","Congo","Congo","Cook Islands","Costa Rica","C\ufffdte dIvoire","Croatia","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands (Malvinas)","Faroe Islands","Fiji","Finland","France","French Guiana","French Polynesia","French Southern Territories","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guadeloupe","Guam","Guatemala","Guernsey","Guinea","Guinea-Bissau","Guyana","Haiti","Heard Island and McDonald Islands","Holy See (Vatican City State)","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Korea","Korea","Kuwait","Kyrgyzstan","Lao People","Latvia","Lebanon","Lesotho","Liberia","Libyan Arab Jamahiriya","Liechtenstein","Lithuania","Luxembourg","Macao","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Martinique","Mauritania","Mauritius","Mayotte","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauru","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Niue","Norfolk Island","Northern Mariana Islands","Norway","Oman","Pakistan","Palau","Palestinian Territory","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Pitcairn","Poland","Portugal","Puerto Rico","Qatar","R\ufffdunion","Romania","Russian Federation","Rwanda","Saint Barthlemy","Saint Helena","Saint Kitts and Nevis","Saint Lucia","Saint Martin","Saint Pierre and Miquelon","Saint Vincent and the Grenadines","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Georgia and the South Sandwich Islands","Spain","Sri Lanka","Sudan","Suriname","Svalbard and Jan Mayen","Swaziland","Sweden","Switzerland","Syrian Arab Republic","Taiwan","Tajikistan","Tanzania","Thailand","Timor-Leste","Togo","Tokelau","Tonga","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Turks and Caicos Islands","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States","United States Minor Outlying Islands","Uruguay","Uzbekistan","Vanuatu","Venezuela","Viet Nam","Virgin Islands, British","Virgin Islands, U.S.","Wallis and Futuna","Western Sahara","Yemen","Zambia","Zimbabwe"],"date_from":"","date_to":""}',true);
    $filter_data = json_decode(file_get_contents('csv/filter.json'),true);
    
    ttt("start");
	$conn = new Mongo('localhost');
	$mongodb = $conn->users;
    $date_from =  new MongoDate(strtotime($filter_data["date_from"]));
    MongoCursor::$timeout = -1;
    $date = new DateTime($filter_data["date_to"]);
    $date->add(new DateInterval('P1D'));
    $date_to_new = $date->format('Y-m-d H:i:s');
    $date_to =  new MongoDate(strtotime($date_to_new));
	ttt("after mongo connection");
	if($filter_data["selectallapps"]){
ttt("if count");
  //$collection = $mongodb->device_unique;		  
  $collection = $mongodb->main_device;
	    $count=$collection->find(
                     array('$and' => 
                            array(
                                    array("country_code" => array('$in' => $filter_data["country"])),
                                    array("device" => array('$in' => $filter_data["platform"])),
                                        (
                                            isset($filter_data["date_from"])
                                            ?array("updated_at" =>array('$gt' => $date_from, '$lte' => $date_to)                                  )
                                            :array("updated_at" =>array())
                                        ),
                                    
                                )
                        )  
                    )->count();
	ttt("if count end");
	}else{
		ttt("else count");
	    //$collection = $mongodb->application_unique;	
	    $collection = $mongodb->main_application;
	    $count=$collection->find(
                     array('$and' => 
                            array(
                                   array("app_name" => array('$in' => $filter_data["app_name"])),
                                    array("country_code" => array('$in' => $filter_data["country"])),
                                    array("device" => array('$in' => $filter_data["platform"])),
                                        (
                                            isset($filter_data["date_from"])
                                            ?array("updated_at" =>array('$gt' => $date_from, '$lte' => $date_to)                                  )
                                            :array("updated_at" =>array())
                                        ),
                                )
                        )  
                    )->count();
	   ttt("else count end");
	}
	ttt("after".$count);
	$no_of_records=50000;
	$no_of_csv=$count/$no_of_records;
	$no_of_csv= ceil($no_of_csv);
	$folder=date("m-d-Yh:i:s");
	if (!file_exists('csv/'.$folder)){
    		mkdir('csv/'.$folder, 0777, true);
	}
	//echo $no_of_csv; die("no of loop");
    	for($i=0;$i<$no_of_csv;$i++){
    	    echo $i;
            echo "<hr>";
    	    $csv_data = getfiltereddata($filter_data,$i*$no_of_records,$no_of_records);
            ttt("after csv data");
    	    $csvname='/'.date("m-d-YH:i:s")."csv";
    	    
    	    //header('Content-Type: application/csv; charset=utf-8');
    	    //header('Content-Disposition: attachment; filename=app_records.csv');                    
    	    //header("Content-Transfer-Encoding: UTF-8");
	    
    	    $output = @fopen("csv/".$folder."/app_records_".$i.".csv", 'w');
    	    
    		  
    	    if($filter_data["selectallapps"]){
    	     //if(true){
        		$heading_arr = array('Device ID' , "OS Version", "Platform", "Country", "Device Type", "Device Maker", "Device Model");
        		for($j=1;$j<=400;$j++){
        		    $heading_arr[] = "App_".$j;
        		}
        		fputcsv($output, $heading_arr);
        		foreach($csv_data as $data){

        			$main_arr = array($data["device_id"],$data["os_ver"],$data["device"],$data["country_code"],$data["device_type"],$data["maker"],$data["model"]);
        			$apps = explode(", ",$data["app_data"]);
        			foreach($apps as $app){
        			    $main_arr[]=$app;
        			} 
        			fputcsv($output,$main_arr);  
        		    }
    		 
    	    }else{
        		fputcsv($output, array('Device ID', 'Application Name' , "OS Version", "Platform", "Country", "Device Type", "Device Maker", "Device Model",  "Date Installed" ));
        		foreach($csv_data as $data){
        			$main_arr = array($data["device_id"],$data["app_name"],$data["os_ver"],$data["device"],$data["country_code"],$data["device_type"],$data["maker"],$data["model"],$data["date_time_installed"]);
        			fputcsv($output,$main_arr );  
                 
        		    }
    	    }
        
    		fclose($output);
            //echo "<pre>"; print_r($csv_data); 
    	    //$this->_redirector->gotoSimple('apprecords', 'csv','admin');
    	 
    	}

	send_csv_mail();
	change_csv_status();
	
	ttt("ok done");
	
        }catch(Exception $e){
           echo "<pre>"; print_r($e); die;
        }
    }


function getfiltereddata($data,$skip,$limit){
    
    $conn = new Mongo('localhost');
    $mongodb = $conn->users;
    
    try{
        if($data){  
            $params = $data;
            $date_from =  new MongoDate(strtotime($params["date_from"]));
            
            $date = new DateTime($params["date_to"]);
            $date->add(new DateInterval('P1D'));
            $date_to_new = $date->format('Y-m-d H:i:s');
            $date_to =  new MongoDate(strtotime($date_to_new));
        //echo date('Y-m-d H:i:s', strtotime('+1 day', '2015-04-15 00:00:00'));
           if($data["selectallapps"]){
        //if(true){
                unset($data["app_name"]);
                $collection = $mongodb->main_device;
                //$collection = $mongodb->device_unique;
                $csv_data = $collection->find(
                     array('$and' => 
                            array(
                                    array(
                                            "country_code" => array('$in' => $params["country"])
                                        ),
                                    array(
                                            "device" => array('$in' => $params["platform"])
                                        ),
                                        (
                                            isset($params["date_from"])
                                            ?array("updated_at" =>array('$gt' => $date_from, '$lte' => $date_to)                                  )
                                            :array("updated_at" =>array())
                                        ),
                                    
                                )
                        )  
                    )->skip($skip)->limit($limit)->timeout(-1);
              
                    foreach($csv_data as $data){

                        foreach($data["app_data"] as $apps){
                                $app_names[] =  $apps["app_name"];
                        }

                        $data["app_data"]= implode(", ",$app_names);
                        $result[] = $data;
                        unset($app_names);
                    }
            }else{
        
                $collection = $mongodb->main_application;
		//$collection = $mongodb->application_unique;
                $csv_data = $collection->find(
                     array('$and' => 
                            array(
                                   array(
                                            "app_name" => array('$in' => $params["app_name"])                       
                                        ),
                                    array(
                                            "country_code" => array('$in' => $params["country"])
                                        ),
                                    array(
                                            "device" => array('$in' => $params["platform"])
                                        ),
                                        (
                                            isset($params["date_from"])
                                            ?array("updated_at" =>array('$gt' => $date_from, '$lte' => $date_to)                                  )
                                            :array("updated_at" =>array())
                                        ),
                                )
                        )  
                    )->skip($skip)->limit($limit)->timeout(-1);
                foreach($csv_data as $data){

                       $result[] = $data; 
                    }

            } 
                    //)->limit(1000);
                    ttt("in data genration");
                    return @$result;
                  
            }
        }catch(Exception $e){
           echo "<pre>"; print_r($e); die;
        }
        


}

function ttt($stringData){

   $date=date("l, d F, Y, H:I");
      $updatefile = "userlogs.log";
      //$fh = fopen($updatefile, 'a') or die("can't open file");

      //$stringData = " Thisi is the String data ";
      //fwrite($fh, $stringData);
      //fclose($fh);


       $fd = fopen($updatefile, 'a') or die("can't open file");
         // append date/time to message
         $str = "[" . date("Y/m/d h:i:s", mktime()) . "] " . $stringData; 
         // write string
         fwrite($fd, $str . "\n");
         // close file
         fclose($fd);

}
    function send_csv_mail(){
		    require 'PHPMailer-master/PHPMailerAutoload.php';
	    	    $mail = new PHPMailer;
	            $mail->IsSMTP();
	            $mail->CharSet = 'UTF-8';
	            $mail->Host       = "email-smtp.us-west-2.amazonaws.com"; // SMTP server example
	            $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
	            $mail->SMTPAuth   = 'true';                  // enable SMTP authentication
	            $mail->SMTPSecure = 'tls';   		// Enable encryption, 'ssl' also accepted
		    $mail->Port       = 25;                    // set the SMTP port for the GMAIL server
		    $mail->Username   = "AKIAIZPYXXUP7A4DOIIA"; // SMTP account username example
		    $mail->Password   = "Al8Azs+UKHvXhowFQUj34VaUN8SJOSkw/BBycORQ1Zpv";        // SMTP account password example
		    $mail->From = 'mss.guneet@gmail.com';     //Set who the message is to be sent from
		    $mail->addReplyTo('mss.guneet@gmail.com', 'Guneet kaur');  //Set an alternative reply-to address
		    $mail->addAddress('mss.vikrantbhalla@gmail.com', 'Vikrant Bhalla');  // Add a recipient
		    $mail->addAddress('mss.guneet@gmail.com', 'Guneet kaur');               // Name is optional
		    //$mail->addAddress('daniel@mobadagency.com','daniel');  
		    $mail->isHTML(true);                                  // Set email format to HTML 
		    $mail->Subject = 'Csv Generated on server';
		    $mail->Body =  '<html>
			<body topmargin="25">
				<p> New CSV has been gererated on server.Please login to FTP and download.</p>
			</body>
			</html>';
		    $mail->AltBody = 'CSV generated on server';


		    if(!$mail->send()) {
			echo 'Message could not be sent.';
			 echo 'Mailer Error: ' . $mail->ErrorInfo;
			} else {
			echo 'Message has been sent';
			}

		}
	 function change_csv_status(){
		$servername = "localhost";
		$username = "root";
		$password = "root";

		// Create connection
		$conn = mysql_connect($servername, $username, $password);

		if (!$conn) {
    			die('Could not connect: ' . mysql_error());
		}

		$db=mysql_select_db('mydb',$conn);
		$query="update csv_status_tbl set status = 'complete' where id=1";
		$result=mysql_query($query);
		
	}
ttt();

csvgenrateAction();

?>    

