<?php

class AdminController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        session_start();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $checklogin = $_SESSION["login_data"];
        if( count($_SESSION["login_data"]) == 0 ){
            $this->_redirector->gotoSimple('index', 'index');
        }
    }

    public function indexAction()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select_clients = $db->select()->from('users_tbl')->where('roles = ?','1');
	$getData_clients = $db->fetchAll($select_clients);
	//print_r($getData_clients); exit;
	$this->view->data = array('user' => $getData_clients);
        
    }

    public function csvAction()
    {
        // action body
    }

	public function segmentsAction()
    {
        // action body
    }
    
    /* add new client */
     public function addnewclientAction()
    {
	$db = Zend_Db_Table::getDefaultAdapter();
	if($this->getRequest()->isPOST()) {
	    $email = $db->fetchAll("select * from users_tbl where email=?",array($_POST['email']),2);
	    if(empty($email)){  
	    $data= array(
		"first_name"=> $_POST['first_name'],
		"last_name"=> $_POST['last_name'],
		"email"=> $_POST['email'],
		"password"=>md5($_POST['password']),
		"status"=> '1',
		"roles"=> '1',
			 );
	$n= $db->insert('users_tbl',$data);
	if($n){
	    $this->view->msg="<div  class='alert alert-success'>Client Created Sucessfully</div>";
	}else{
	    $this->view->msg="<div  class='alert alert-success'>Client Not Created /div>";
	}
	}else{
	    $this->view->msg="<div  class='alert alert-danger'>Client already exist</div>";
	}
	
	
    }}
    
    /* edit client information */
     public function editclientAction()
    {
	$db = Zend_Db_Table::getDefaultAdapter();
	$id=$this->getRequest()->getParam('id');
	if($this->getRequest()->isGET()) {
	    
	    $user=$db->fetchAll("select * from users_tbl where id=?",array($id),2);
	    $this->view->data=array("user"=>$user);
	}
	if($this->getRequest()->isPOST()) {
	    
	 if($_POST['password']) {
	    $data= array(
		"first_name"=> $_POST['first_name'],
		"last_name"=> $_POST['last_name'],
		"email"=> $_POST['email'],
		"password"=>md5($_POST['password']),
		"status"=> $_POST['status']
		
			 );
	 }else{
	    $data= array(
		"first_name"=> $_POST['first_name'],
		"last_name"=> $_POST['last_name'],
		"email"=> $_POST['email'],
		"status"=> $_POST['status']
		
			 );
	 }
	$n= $db->update('users_tbl',$data,'id ='.$id);
	if($n){
	    $this->view->msg="<div  class='alert alert-success'>Client Information Updated Sucessfully</div>";
	    $user=$db->fetchAll("select * from users_tbl where id=?",array($id),2);
	    $this->view->data=array("user"=>$user);
	}else{
	     $this->view->msg="<div  class='alert alert-warning'>Opps! Not update !!</div>";
	     $user=$db->fetchAll("select * from users_tbl where id=?",array($id),2);
	    $this->view->data=array("user"=>$user);
	}
	}
    
    }
    
    /* for deactivating any client */
    public function deactivateAction()
    {
	$db = Zend_Db_Table::getDefaultAdapter();
	$param=$this->getRequest()->getParam('id');
	$select_clients_id = $db->select()->from('users_tbl')->where('id = ?',$param) ;
        $getData_clients_id = $db->fetchRow($select_clients_id);
	if($getData_clients_id['status'] == 0){
	   $status='1';	    
	}else{
	   $status='0';	 
	}
	 $data=array(
			'status' => $status
			);
	 $db->update('users_tbl',$data,'id = '.$param);
	 echo $status;
	die; 
    }
    
    /* delete client */
    public function deleteclientAction(){
    
    $db = Zend_Db_Table::getDefaultAdapter();
    $id=$this->getRequest()->getParam('id');
    $n=$db->delete('users_tbl','id='.$id);
    if($n){
	$this->view->msg ='Client deleted successfully !';
        $urlOptions = array( 'controller'=>'admin', 'action'=>'index');
        $this->_helper->redirector->gotoRoute($urlOptions);
    }else{
	$this->view->msg ='Unable to delete client, kindly retry !';
    }
    }


}




