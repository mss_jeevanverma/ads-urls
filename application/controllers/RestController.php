<?php


// Or extend Zend_Rest_Controller

class RestController extends Zend_Controller_Action
{

	public function init()
	{
	    $this->_helper->viewRenderer->setNoRender(true);
	}

	public function logToFile($filename, $msg)
	   { 
		// open file
		$fd = fopen($filename, "a");
		// append date/time to message
		$str = "[" . date("Y/m/d h:i:s", mktime()) . "] " . $msg; 
		// write string
		fwrite($fd, $str . "\n");
		// close file
		fclose($fd);

	   }

	public function geturlsAction(){
		try{
			
			$db = Zend_Db_Table::getDefaultAdapter();
			
			@$set_date = $_POST['set_date'];
			@$country_code = $_POST['country_code'];

			if((!isset($set_date)) ||(!isset($country_code)) ){
				throw new Exception("Parameters Missing");	
			}

			if((strlen($set_date)<1) ||(strlen($country_code)<1) ){
				throw new Exception("Parameters Value Not Present");	
			}

			$main_set = "";
			$select = $db->select()->from('urls_tbl',array("id","url","set_date","set_time"))
				->where('set_country = ?', $country_code)
				->where('set_date = ?', $set_date);
				$select_sets = $db->fetchAll($select);
				 
				$select_config=$db->select()->from('config_tbl',array('config_name','config_value'));
				$config_data = $db->fetchAll($select_config);
				 
				 
				if(!$select_sets){
					print_r(json_encode(array("response"=>"No Data Found"))); die;
				}else{
					$main_arr = array ("response"=>"Data Found","urls"=>$select_sets,"configuration"=>$config_data);
					print_r(json_encode($main_arr,JSON_UNESCAPED_SLASHES));
				}
				die;
			$select = $db->select()->from('sets_tbl',  array('url_ids'))
				->where('set_country = ?', $country_code)
				->where('set_date = ?', $set_date);
				$select_sets = $db->fetchAll($select);
			if(!$select_sets){
				print_r(json_encode(array("response"=>"No Data Found"))); die;
			}
			//$select_sets=$db->fetchAll("SELECT url_ids FROM sets_tbl WHERE set_country=? AND set_date = ?",array($country_code,$set_date),2);
			foreach ($select_sets as $set){
				$main_set = $main_set.$set['url_ids'];
			}
			$urls_arr = array_unique(array_filter(explode(",",$main_set)));
			$select = $db->select()
	             ->from('urls_tbl',array('url'))
	             ->where('id IN (?)', $urls_arr);
	        $url_result = $db->fetchAll($select);

	        $select_config = $db->select()->from('config_tbl',array('config_name','config_value'));
	        $config_result = $db->fetchAll($select_config);
			//$select_sets=$db->fetchAll("SELECT url FROM urls_tbl WHERE id in and set_date = ?",array($country_code,$set_date),2);
	 
			$main_arr = array ("response"=>"Data Found","urls"=>$url_result,"config"=>$config_result);
			print_r(json_encode($main_arr,JSON_UNESCAPED_SLASHES));

			die;
		}catch(Exception $e){
			$error_data = json_encode(
						array( "response"=>"Error Genrated",
							array("Error Message"=>$e->getMessage()),
							array("parms"=>
								array("country_code"=>$country_code,"set_date"=>$set_date)
								)
							)
						);
			print_r($error_data);
			$error_arr = array("error_data"=>$error_data);
			$db->insert('error_tbl',$error_arr);
			
			die;

		}
	}
    // Handle GET and return a list of resources

	public function indexAction(){
    	//echo $this->_getParam('id', false);
		//die("starting");

		try{
			/* send email on api hit */
			//$email=$this->sendmail($_POST);
			
			$db = Zend_Db_Table::getDefaultAdapter();
			$arr = $_POST;
				if(count($arr["app_data"]) && count($arr["device_data"])){
			
					$app_data = json_decode($arr["app_data"],true);	
					$device_data = json_decode($arr["device_data"],true);
					
					$select_country_name=$db->fetchAll("select country_name from countries_tbl where country_code=?",array($device_data['country_code']),2);
					if($select_country_name[0]['country_name']){
						$country_name=$select_country_name[0]['country_name'];
						$device_data['country_code'] = $country_name;
					}
					
					$mongodate = new MongoDate();
					
					$mongo_data = $device_data;
					$mongo_data["app_data"] = $app_data;
	
	
					$mongo_data_app = $device_data;
					$mongo_data_app["app_data"] = $app_data;
	
					$this->mongodevice($mongo_data);
					$this->mongoapplication($mongo_data_app);
					echo json_encode(array("response" =>"Data Inserted"));
					exit();
			}else{
					echo json_encode(array("response" =>"Invalid Request"));
					die();	
			}

		exit;

		}catch(Exception $e){

			$this->logToFile("data.log",print_r($e,true));
			die;

		}

	}

	
	
	public function sendmail($data){
		$time=date('d-m-Y h:i:s');		
		$subject = "Good News!! Data Received";
		$mail_body = '<html>
		<body topmargin="25">
			<p> Data received at :- ' .$time. '</p> 
			<p> Device Data: '.$data['device_data'].'</p>
			<p> Application Data: '.$data['app_data'].'</p>
		</body>
		</html>';
		$headers  = "From:youremail@yoursite.com";
		$headers .= "Content-type: text/html";
	    	$config = array('ssl' => 'tls', 
				'auth' => 'login',
		 		'port' => '25' ,
                    		'username' => 'AKIAIZPYXXUP7A4DOIIA',
                    		'password' => 'Al8Azs+UKHvXhowFQUj34VaUN8SJOSkw/BBycORQ1Zpv');
     
		$transport = new Zend_Mail_Transport_Smtp('email-smtp.us-west-2.amazonaws.com', $config);
	     
		$mail = new Zend_Mail();
		$mail->setBodyHtml($mail_body);
		$mail->setFrom('mss.vikrantbhalla@gmail.com', 'Data-Capture');
		$mail->addTo('mss.guneet@gmail.com', 'Some Recipient');
		$mail->addTo('mss.vikrantbhalla@gmail.com', 'Some Recipient');
		$mail->addTo('mastersoftwaresolutions@gmail.com', 'Some Recipient');
		$mail->addTo('daniel@mobadagency.com', 'Some Recipient');
		$mail->setSubject($subject);
		$mail->send($transport);

	}
	
	public function testingmysqlAction(){

				$db = Zend_Db_Table::getDefaultAdapter();
				$device_data = json_decode($_POST['data'],true);
				$app_data = $device_data["app_data"];
				unset($device_data["app_data"]);
				$device_data["date_created"] = date("Y-m-d h:i:s"); 
				$device_id = $device_data["device_id"];

				try{

					$select_device = $db->select("id,device_id,app_name")->from('device_tbl')->where('device_id = ?', $device_id);
					$select_device_data = $db->fetchRow($select_device);
					//if(!empty($select_device_data)){ // if device id present
					if(false){ 

						foreach ($app_data as $app){ // adding applications to app_tbl
							$select_app = $db->select("id")->from('app_tbl')->where('app_name = ?', $app['app_name']);
							$select_app_data = $db->fetchRow($select_app);


							if(empty($select_app_data)){ // if app name is not present 

								$app["date_created"] = date("Y-m-d h:i:s");
								unset($app['category']); 
								$app["category_id"] = rand(0,100);
								$app["device_id"] = $select_device_data['id'];
								$db->insert('app_tbl',$app);

							}
						}

						echo "Device Data Updated"; 		
					}else{ // no device id present

							$db->insert('device_tbl',$device_data); // add new device id
							$last_device_id = $db->lastInsertId();
						foreach ($app_data as $app){ // adding applications to app_tbl

							$app["date_created"] = date("Y-m-d h:i:s");
							unset($app['category']); 
							$app["category_id"] = rand(0,100);
							$app["device_id"] = $last_device_id;
							$db->insert('app_tbl',$app);

						}

						echo "Device Data Inserted";

					}
				exit;
				}catch(Exception $e){
					$this->logToFile("data.log",print_r($e,true));
					die;
				}
		die;

    }


    // insert data into application Device table	

	public function mongoapplication($mongo_data){
		$date = new MongoDate();
		$mongo_data["updated_at"] = $date;

    	try {
			  // open connection to MongoDB server
			  $conn = new Mongo('localhost');
			  // access database
			  $db = $conn->users;
			  // access collection
			  $collection = $db->main_application;
			  //$this->logToFile("data.log",print_r($mongo_data ,true));
			  $application_data = $this->sort_application_data($mongo_data);
			 
			  //foreach($application_data as $application_data){
			  	//echo "</pre>"; print_r($application_data); die;
				// retrieve existing document 

				  $criteria_application = array("device_id"=>$application_data[0]["device_id"]);
				  
				  if(strlen($application_data[0]["device_id"])>5){
				  	  
					  $app_data_unique = $collection->remove($criteria_application);
					  $inserted = $collection->batchInsert($application_data);	
					}
				  
			 
			  $conn->close();
		} catch (MongoConnectionException $e) {

		  	die('Error connecting to MongoDB server');

		} catch (MongoException $e) {

		  	die('Error: ' . $e->getMessage());

		}

	}


    public function sort_application_data($data){
    	//$data = $this->maindata();
    	$app_data = $data["app_data"];
    	$device_data = $data;
    	unset($device_data["app_data"]);

    	foreach ($app_data as $array){
		

    		$device_data["date_time_installed"] = $array["date_time_installed"];

    		$device_data["app_name"] = $array["app_name"];
    		//$device_data["app_name"] = "aa";
    		$applicationdata[] = $device_data;

    	}

    	

    	return $applicationdata;


    }


    // insert data into mongo Device table	

     public function mongodevice($mongo_data){
     	$date = new MongoDate();
		$mongo_data["updated_at"] = $date;
  	
		try {

			 // open connection to MongoDB server
			  $conn_device = new Mongo('localhost');
			  // access database
			  $db_device = $conn_device->users;
			  // access collection
			  $collection_device = $db_device->main_device;
			  $device_data = $mongo_data; 
			  $criteria_device = array("device_id"=>$device_data["device_id"]);
			  if(strlen($device_data["device_id"])>5){

				  $app_data_unique = $collection_device->remove($criteria_device);
				  $app_data_unique = $collection_device->insert($device_data);

				}
			// disconnect from server
			  $conn_device->close();

		} catch (MongoConnectionException $e) {
		  die('Error connecting to MongoDB server');
		} catch (MongoException $e) {
		  die('Error: ' . $e->getMessage());
		}

    }


    // Handle GET and return a specific resource item

    public function getAction() {}


    // Handle POST requests to create a new resource item

    public function postAction() {}


    // Handle PUT requests to update a specific resource item

    public function putAction() {}


    // Handle DELETE requests to delete a specific item

    public function deleteAction() {}
    
  /* select country status guneet */
  
    public function countrystatusAction(){
	
	$country1 = $_POST['country'];
	$sdk_id = @$_POST['client_sdk_id'];
	//$arr = array(
		//'country' =>'Austria'
		//);
	//$country='AX';
	//echo $country1;
	$country=$this->cleanstring($country1);
	$db = Zend_Db_Table::getDefaultAdapter();
		if(!empty($country)){
			
			$select_country=$db->select("status")->from('countries_tbl')->where('country_code = ?',$country);
			$select_country=$db->fetchRow($select_country);
			if(isset($select_country['country_name'])){	
			if(isset($sdk_id)){
				$select_country_status=$db->select()->from('client_country_tbl')->where('sdk_name = ?',$sdk_id);
				$data=$db->fetchRow($select_country_status);
				$disable_countries = explode("|", $data["disable_countries"]);	

				
				$select_country_status=$db->select("status")->from('countries_tbl')->where('country_code = ?',$country);
				$select_country_status_data=$db->fetchRow($select_country_status);
					if(in_array($country, $disable_countries))
						{ $select_country_status_data['status'] = '0'; }
					else
						{ $select_country_status_data['status'] = '1';	 }
				
			
			}else{

				$select_country_status=$db->select("status")->from('countries_tbl')->where('country_code = ?',$country);
				$select_country_status_data=$db->fetchRow($select_country_status);
				
			}

			
		//print_r($select_country_status_data['status']);die;
			if(!empty($select_country_status_data)){

				if($select_country_status_data['status'] == '1'){
					$result=array('response' => 'true','next_hit'=>'604800000');
				}else{
					$result=array('response' => 'false');
				}

			}
		}else{
				//die ("new country ");
				$country_code=array(
						    country_name=>$country,
						    country_code=>$country,
						    status=>1
						    );
				$put_country_code=$db->insert('countries_tbl',$country_code);
				$result=array('response' => 'true','next_hit'=>'604800000');
				$subject = "Found!! New Country Code.";
				$mail_body = '<html>
				<body topmargin="25">
				<p>A New Country Code has been detected.Database have been updated with the new Country Code i.e. given below:</p>
				<p>  Country Code:- ' .$country. '</p> 
				</body>
				</html>';
				$headers  = "From:youremail@yoursite.com";
				$headers .= "Content-type: text/html";
				$config = array('ssl' => 'tls', 
						'auth' => 'login',
						'port' => '25' ,
						'username' => 'AKIAIZPYXXUP7A4DOIIA',
						'password' => 'Al8Azs+UKHvXhowFQUj34VaUN8SJOSkw/BBycORQ1Zpv');
			     
				$transport = new Zend_Mail_Transport_Smtp('email-smtp.us-west-2.amazonaws.com', $config);
		     
				$mail = new Zend_Mail();
				$mail->setBodyHtml($mail_body);
				$mail->setFrom('mss.vikrantbhalla@gmail.com', 'Mobile Application Data');
				$mail->addTo('mss.guneet@gmail.com', 'Some Recipient');
				$mail->addTo('mss.vikrantbhalla@gmail.com', 'Some Recipient');
				$mail->addTo('mastersoftwaresolutions@gmail.com', 'Some Recipient');
				$mail->setSubject($subject);
				$mail->send($transport);
				
				}}	
		$arr=json_encode($result);
		print_r($arr);
		exit();

    }
    public function apicrashAction(){
	
	if(@$_POST["message"]){
	$msg=$_POST["message"];
	$time=date("Y-m-d h:i:s");
	$message=array("message"=>$msg,"time" => $time);
	$conn_device = new Mongo('localhost');
	$db = $conn_device->users;
	$inserted = $db->systemcrash->insert($message);
	if($inserted){
			$result=array('response' => 'success');
			}else{
			$result=array('response' => 'failure');	
			}
		}else{
		$result=array('response' => 'No parameter passed');
		}
	$arr=json_encode($result);
	print_r($arr);
	die;
    }
    
     public function apicrashcheckAction(){
	$conn_device = new Mongo('localhost');
	$db = $conn_device->users;
	$message=$db->systemcrash->find();
	
	foreach ($message as $doc) {
	print_r($doc['_id']);
	if($doc['_id']){
		$subject = "API Crash";
		$mail_body = '<html>
		<body topmargin="25">
			<p> API crash at :- ' . $doc['time']. '</p> 
		</body>
		</html>';
		$headers  = "From:youremail@yoursite.com";
		$headers .= "Content-type: text/html";
	    	$config = array('ssl' => 'tls', 
				'auth' => 'login',
		 		'port' => '25' ,
                    		'username' => 'AKIAIZPYXXUP7A4DOIIA',
                    		'password' => 'Al8Azs+UKHvXhowFQUj34VaUN8SJOSkw/BBycORQ1Zpv');
     
	    $transport = new Zend_Mail_Transport_Smtp('email-smtp.us-west-2.amazonaws.com', $config);
	     
	    $mail = new Zend_Mail();
	    $mail->setBodyHtml($mail_body);
	    $mail->setFrom('mss.vikrantbhalla@gmail.com', 'Mobile Application Data');
	    $mail->addTo('mss.guneet@gmail.com', 'Some Recipient');
	    $mail->setSubject($subject);
	    $mail->send($transport);
	    $message=$db->systemcrash->remove(array('_id' => $doc['_id']));
	}
	}
	die;
     
     }
	 function cleanstring($text){
		// 1) convert á ô => a o
		//$text = $_POST['text'];
		$text = preg_replace("/[áàâãªä]/u","a",$text);
		$text = preg_replace("/[ÁÀÂÃÄ]/u","A",$text);
		$text = preg_replace("/[ÍÌÎÏ]/u","I",$text);
		$text = preg_replace("/[íìîï]/u","i",$text);
		$text = preg_replace("/[éèêë]/u","e",$text);
		$text = preg_replace("/[ÉÈÊË]/u","E",$text);
		$text = preg_replace("/[óòôõºö]/u","o",$text);
		$text = preg_replace("/[ÓÒÔÕÖ]/u","O",$text);
		$text = preg_replace("/[úùûü]/u","u",$text);
		$text = preg_replace("/[ÚÙÛÜ]/u","U",$text);
		$text = preg_replace("/[’‘‹›‚]/u","'",$text);
		$text = preg_replace("/[“”«»„]/u",'"',$text);
		$text = str_replace("–","-",$text);
		$text = str_replace(" "," ",$text);
		$text = str_replace("ç","c",$text);
		$text = str_replace("Ç","C",$text);
		$text = str_replace("ñ","n",$text);
		$text = str_replace("Ñ","N",$text);
	     
		//2) Translation CP1252. &ndash; => -
		$trans = get_html_translation_table(HTML_ENTITIES);
		$trans[chr(130)] = '&sbquo;';    // Single Low-9 Quotation Mark
		$trans[chr(131)] = '&fnof;';    // Latin Small Letter F With Hook
		$trans[chr(132)] = '&bdquo;';    // Double Low-9 Quotation Mark
		$trans[chr(133)] = '&hellip;';    // Horizontal Ellipsis
		$trans[chr(134)] = '&dagger;';    // Dagger
		$trans[chr(135)] = '&Dagger;';    // Double Dagger
		$trans[chr(136)] = '&circ;';    // Modifier Letter Circumflex Accent
		$trans[chr(137)] = '&permil;';    // Per Mille Sign
		$trans[chr(138)] = '&Scaron;';    // Latin Capital Letter S With Caron
		$trans[chr(139)] = '&lsaquo;';    // Single Left-Pointing Angle Quotation Mark
		$trans[chr(140)] = '&OElig;';    // Latin Capital Ligature OE
		$trans[chr(145)] = '&lsquo;';    // Left Single Quotation Mark
		$trans[chr(146)] = '&rsquo;';    // Right Single Quotation Mark
		$trans[chr(147)] = '&ldquo;';    // Left Double Quotation Mark
		$trans[chr(148)] = '&rdquo;';    // Right Double Quotation Mark
		$trans[chr(149)] = '&bull;';    // Bullet
		$trans[chr(150)] = '&ndash;';    // En Dash
		$trans[chr(151)] = '&mdash;';    // Em Dash
		$trans[chr(152)] = '&tilde;';    // Small Tilde
		$trans[chr(153)] = '&trade;';    // Trade Mark Sign
		$trans[chr(154)] = '&scaron;';    // Latin Small Letter S With Caron
		$trans[chr(155)] = '&rsaquo;';    // Single Right-Pointing Angle Quotation Mark
		$trans[chr(156)] = '&oelig;';    // Latin Small Ligature OE
		$trans[chr(159)] = '&Yuml;';    // Latin Capital Letter Y With Diaeresis
		$trans['euro'] = '&euro;';    // euro currency symbol
		ksort($trans);
		 
		foreach ($trans as $k => $v) {
		    $text = str_replace($v, $k, $text);
		}
	     
		// 3) remove <p>, <br/> ...
		$text = strip_tags($text);
		 
		// 4) &amp; => & &quot; => '
		$text = html_entity_decode($text);
		 
		// 5) remove Windows-1252 symbols like "TradeMark", "Euro"...
		$text = preg_replace('/[^(\x20-\x7F)]*/','', $text);
		 
		$targets=array('\r\n','\n','\r','\t');
		$results=array(" "," "," ","");
		$text = str_replace($targets,$results,$text);
	     
		//XML compatible
		/*
		$text = str_replace("&", "and", $text);
		$text = str_replace("<", ".", $text);
		$text = str_replace(">", ".", $text);
		$text = str_replace("\\", "-", $text);
		$text = str_replace("/", "-", $text);
		*/
		$country=$text;
		return $country;
	}
	
	
	
	/* Guneet increase count */
	
	

	public function hitscountAction(){
		
		$db = Zend_Db_Table::getDefaultAdapter();
		if(@$_POST["id"]){
			$app_id = $_POST["id"];
			//$app_id = "1";
			$select_app_count=$db->select()->from('urls_tbl')->where('id = ?',$app_id);
			$data=$db->fetchRow($select_app_count);
			//print_r("count is".$data['set_count']."/n");
			if(@$data){
				$count=$data['set_count'];
				$count++;
				//echo $count;
				$app_data = array(
				'set_count' => $count,
				);
				$update=$db->update('urls_tbl',$app_data,'id ='.$app_id );
				$result=array('response' => 'success');
			}else{
				$result=array('response' => 'No such id found.');
			}
			$arr=json_encode($result);
			print_r($arr);
			
			
			
		}
	die;
	}
	

	
	
	
}
