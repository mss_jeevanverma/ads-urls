<?php

class Admin_SetsController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        session_start();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $checklogin = $_SESSION["login_data"];
        if($checklogin["roles"]==1){
            $this->_redirector->gotoSimple('index', 'index','index');
        }
        if( count($_SESSION["login_data"]) == 0 ){
            $this->_redirector->gotoSimple('index', 'index','index');
        }
        if( count($_SESSION["login_data"]) == 0 ){
            $this->_redirector->gotoSimple('index', 'index');
        }
	

    }

  

    public function indexAction()
    {       
        $db = Zend_Db_Table::getDefaultAdapter();

       $getData_sets = $db->fetchAll("select sets_tbl.id, sets_tbl.set_name, sets_tbl.set_date, countries_tbl.country_name from sets_tbl,countries_tbl where sets_tbl.set_country = countries_tbl.country_code",array(),2);    

	   $this->view->data = array('sets' => $getData_sets);
       
    }

   
    /* add new Set */
     public function addnewsetAction()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $countries = $db->fetchAll("select * from countries_tbl",array(),2);    
	    
    	if($this->getRequest()->isPOST()) {

            $n= $db->insert('sets_tbl',$_POST);

        	if($n){
        	    $this->view->msg=array('messg'=>"<div  class='alert alert-success'>Set Created Successfully</div>",'success'=>1);

        	}else{
        	    $this->view->msg=array("messg" => "<div  class='alert alert-success'>Client Not Created </div>");
        	}

        }
        $_POST = '';
        $this->view->data=array("countries" => $countries);

    }
    
    /* edit set information */
     public function editsetAction()
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$id=$this->getRequest()->getParam('id');
        $countries = $db->fetchAll("select * from countries_tbl",array(),2);    
    	if($this->getRequest()->isGET()) {
    	    
    	    $set=$db->fetchAll("select * from sets_tbl where id=?",array($id),2);

    	}

    	if($this->getRequest()->isPOST()) {
            $n= $db->update('sets_tbl',$_POST,'id ='.$id);
            $set=$db->fetchAll("select * from sets_tbl where id=?",array($id),2);

            if($n){
                $this->view->msg="<div  class='alert alert-success'>Set Information Updated Successfully</div>";
            }else{
                $this->view->msg="<div  class='alert alert-success'>Somthing Went Wrong , Try Again.</div>";
            }
           
        }
            $this->view->data=array("countries" => $countries,"set"=>$set);    
    }

    
    /* delete set */
    public function deletesetAction(){

        $db = Zend_Db_Table::getDefaultAdapter();
        $id=$this->getRequest()->getParam('id');

        $getData_sets = $db->fetchAll("SELECT * FROM sets_tbl WHERE id = ?",array($id),2);
        $url_ids = $getData_sets[0]["url_ids"];
        $url_ids_arr = explode(",",$url_ids);
        foreach($url_ids_arr as $id_url){
            if($id_url)
            $db->update('urls_tbl',array("set_id"=>""),'id ='.$id_url);
        }
       

        $n=$db->delete('sets_tbl','id='.$id);

        if($n){
    	   $this->view->msg ='Client deleted successfully !';
            $urlOptions = array('module'=>'admin', 'controller'=>'sets', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }else{

    	   $this->view->msg ='Unable to delete client, kindly retry !';
        }
    }

    public function logoutAction()
	{
          session_destroy();
	  $this->_redirector->gotoSimple('index', 'index','index');
	}
        

}

	


