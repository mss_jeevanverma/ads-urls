<?php

class Admin_ReportsController extends Zend_Controller_Action
{

    public function init()
    {
            /* Initialize action controller here */
        session_start();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $checklogin = $_SESSION["login_data"];
        if($checklogin["roles"]==1){
            $this->_redirector->gotoSimple('index', 'index','index');
        }
        if( count($_SESSION["login_data"]) == 0 ){
            $this->_redirector->gotoSimple('index', 'index','index');
        }

    }

    public function indexAction()
    { 
       
        $db = Zend_Db_Table::getDefaultAdapter();

        # Geting daily reports
        $select_daily_reports = $db->select()->from('report_daily')->order(array('id DESC'));
	    $getData_daily_reports = $db->fetchAll($select_daily_reports);

        # Geting daily reports for United States 
        $select_daily_reports_us = $db->select()->from('report_daily_us')->order(array('id DESC'));
        $getData_daily_reports_us    = $db->fetchAll($select_daily_reports_us);

         

       	$this->view->data = array('daily_reports' => $getData_daily_reports,'daily_reports_us' => $getData_daily_reports_us);
    }
    
    
}

