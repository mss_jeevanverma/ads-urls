<?php

class Admin_AssignController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        session_start();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $checklogin = $_SESSION["login_data"];
        if($checklogin["roles"]==1){
            $this->_redirector->gotoSimple('index', 'index','index');
        }
        if( count($_SESSION["login_data"]) == 0 ){
            $this->_redirector->gotoSimple('index', 'index','index');
        }
        if( count($_SESSION["login_data"]) == 0 ){
            $this->_redirector->gotoSimple('index', 'index');
        }
    

    }

  

    public function indexAction()
    {       
        $db = Zend_Db_Table::getDefaultAdapter();

        $getData_urls = $db->fetchAll("select * from urls_tbl ",array(),2);  
        $getData_sets = $db->fetchAll("select * from sets_tbl ",array(),2);    

        $page=$this->_getParam('page',1);
        $paginator = Zend_Paginator::factory($getData_urls);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);
 
        $this->view->paginator=$paginator;  
        $this->view->data = array('urls' => $getData_urls,'sets'=> $getData_sets);
       
    }

   public function updateurlsetAction(){

        $db = Zend_Db_Table::getDefaultAdapter();
        $set_value = $_POST['set_val'];
        $url_value = $_POST['url_val'];
        echo $set_old_val = $_POST['set_old_val'];

        // update the url table 
        $data_url = array ("set_id" => $set_value );
        $db->update('urls_tbl',$data_url,'id ='.$url_value);

        //remove from old set
        $get_set_old = $db->fetchAll("select url_ids from sets_tbl where id = ?",array($set_old_val),2);
        
        if(strlen($get_set_old[0]['url_ids'])>1){
            $ids_in_array_old = explode(',',$get_set_old[0]['url_ids']);
            print_r($ids_in_array_old);
                if(($key = array_search($url_value, $ids_in_array_old)) !== false) {
                    unset($ids_in_array_old[$key]);
                }
            //unset($ids_in_array_old[$url_value]);
            print_r($ids_in_array_old);
            $new_ids_for_old = implode(",", $ids_in_array_old);
            //print_r($new_ids_for_old);
            $data_old = array ("url_ids" => $new_ids_for_old );
            $db->update('sets_tbl',$data_old,'id ='.$set_old_val);
        }

        
        //update the sets table 
        $get_set = $db->fetchAll("select url_ids from sets_tbl where id = ?",array($set_value),2);
        echo "dd=".$get_set[0]['url_ids']."==dd";
     

        $ids_in_array = explode(',',$get_set[0]['url_ids']);
        if(!in_array($url_value, $ids_in_array, true)){
            array_push($ids_in_array, $url_value);
        }
        $new_ids = implode(",", $ids_in_array);

        
        $data = array ("url_ids" => $new_ids );
        $n= $db->update('sets_tbl',$data,'id ='.$set_value);
        die("here");
   }
    /* add new Set */
     public function addnewurlAction()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        if($this->getRequest()->isPOST()) {

            $n= $db->insert('urls_tbl',$_POST);

            if($n){
                $this->view->msg=array('messg'=>"<div  class='alert alert-success'>URL Created Successfully</div>",'success'=>1);

            }else{
                $this->view->msg=array("messg" => "<div  class='alert alert-success'>URL Not Created </div>");
            }

        }
        $_POST = '';
        
    }
    
    /* edit set information */
     public function editurlAction()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $id=$this->getRequest()->getParam('id');
        if($this->getRequest()->isGET()) {
            
            $url=$db->fetchAll("select * from urls_tbl where id=?",array($id),2);

        }

        if($this->getRequest()->isPOST()) {

            $n= $db->update('urls_tbl',$_POST,'id ='.$id);
            print_r($n);
            $url=$db->fetchAll("select * from urls_tbl where id=?",array($id),2);

            if($n){
                $this->view->msg="<div  class='alert alert-success'>URL Information Updated Successfully</div>";
            }else{
                $this->view->msg="<div  class='alert alert-success'>Somthing Went Wrong , Try Again.</div>";
            }
           
        }
            $this->view->data=array("url"=>$url);    
    }

    
    /* delete set */
    public function deleteurlAction(){

        $db = Zend_Db_Table::getDefaultAdapter();
        $id=$this->getRequest()->getParam('id');

        $n=$db->delete('urls_tbl','id='.$id);

        if($n){
            $this->view->msg ='Url deleted successfully !';
            $urlOptions = array('module'=>'admin', 'controller'=>'urls', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }else{

           $this->view->msg ='Unable to delete Url, kindly retry !';
        }
    }

    public function logoutAction()
    {
          session_destroy();
      $this->_redirector->gotoSimple('index', 'index','index');
    }
        

}

    


