<?php

class Admin_CountryController extends Zend_Controller_Action
{
  public function init()
    {
            /* Initialize action controller here */
        session_start();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $checklogin = $_SESSION["login_data"];
        if($checklogin["roles"]==1){
            $this->_redirector->gotoSimple('index', 'index','index');
        }
        if( count($_SESSION["login_data"]) == 0 ){
            $this->_redirector->gotoSimple('index', 'index','index');
        }

    }
    
    public function indexAction()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select_country = $db->select()->from('countries_tbl');
	$getData_country = $db->fetchAll($select_country);
	$select_client=$db->select()->from('client_country_tbl');
	$getdata_client_country=$db->fetchAll($select_client);
	//print_r($getdata_client_country); exit;
	$this->view->data = array('country' => $getData_country,'client'=>$getdata_client_country);
    }
    
    public function countrystatusAction(){
       $db = Zend_Db_Table::getDefaultAdapter();
       if(isset($_POST['id'])) {
	    //echo $_POST['state'];
	     $id=$_POST['id']; 
	     $value=$_POST['value'];
	  }
	  else{
	    $data=json_decode(file_get_contents("php://input"));
	     $id=$data->id;
	     $value=$data->value;
	    
	 }
       
         if($value == 'true'){
            $status = 1;
         }else{
            $status = 0;
         }
         $data=array(
            'status'=>$status
         );
         $n = $db->update('countries_tbl',$data,'id='.$id);
         die;   
     }
     public function clientcountryAction(){
	$client_id=$_POST['client_id'];
	//console.log($client_id);
	//echo $client_id;
	$db = Zend_Db_Table::getDefaultAdapter();
	$select_country = $db->select()->from('countries_tbl');
	$getData_country = $db->fetchAll($select_country);
	//print_r($getData_country);
	$select_client=$db->select()->from('client_country_tbl')->where('client_id = ?', $client_id);
	$getdata_client_country=$db->fetchAll($select_client);
	$this->view->data = array('country' => $getData_country,'client'=>$getdata_client_country);
	
     }
     public function clientcountryupdateAction(){
	try{
	    $country_id= $_POST['country_id'];
	    $client_id=$_POST['client_id'];
	    $db = Zend_Db_Table::getDefaultAdapter();
	    $select_client=$db->select()->from('client_country_tbl')->where('client_id = ?', $client_id);
	    $getdata_client_country=$db->fetchAll($select_client);
	    $old_disable_country=$getdata_client_country[0]['disable_countries'];
	    $arr_disable=explode("|",$old_disable_country);
	    //if (strpos($old_disable_country,$country_id) !== false){
	    if(in_array($country_id,$arr_disable)){
		$key = array_search($country_id,$arr_disable);
		if($key!==false){
		  unset($arr_disable[$key]);
		}
		$remove_disable_country=implode("|",$arr_disable);
		//$remove_disable_country=str_replace($country_id."|", "", $old_disable_country);
		$data3=array(
			    'disable_countries' => $remove_disable_country 
		    );
    
		$db->update('client_country_tbl', $data3, 'client_id = '."'$client_id'");
		exit();
	    }else{
	
	    $new_disable_country=$old_disable_country."|".$country_id;
//echo $new_disable_country;
	    $data4=array(
			    'disable_countries' => $new_disable_country 
		    );
    
		$db->update('client_country_tbl', $data4, 'client_id = '."'$client_id'");
		exit();
	    }
	}catch(Exception $e){
	  print_r($e);
	}
     }

}

?>