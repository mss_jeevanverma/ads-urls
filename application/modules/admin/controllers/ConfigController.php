<?php

class Admin_ConfigController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        session_start();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $checklogin = $_SESSION["login_data"];
        if($checklogin["roles"]==1){
            $this->_redirector->gotoSimple('index', 'index','index');
        }
        if( count($_SESSION["login_data"]) == 0 ){
            $this->_redirector->gotoSimple('index', 'index','index');
        }
        if( count($_SESSION["login_data"]) == 0 ){
            $this->_redirector->gotoSimple('index', 'index');
        }
    

    }

  

    public function indexAction()
    {       
        
        $db = Zend_Db_Table::getDefaultAdapter();

        $getData_config = $db->fetchAll("select * from config_tbl ",array(),2);

        if($this->getRequest()->isPOST()) {
            $first_hit =  $_POST['first_hit'];
            $ads_view_diff =  $_POST['ads_view_diff'];
            $db->update('config_tbl',array("config_value"=>$first_hit),"config_name = 'first_hit'");
            $db->update('config_tbl',array("config_value"=>$ads_view_diff),"config_name = 'ads_view_diff'");
            $this->view->msg="<div  class='alert alert-success'>Configuration Updated Successfully</div>";
        }    

       $this->view->data = array('config' => $getData_config);
       
    }

   

    public function logoutAction()
    {
          session_destroy();
      $this->_redirector->gotoSimple('index', 'index','index');
    }
        

}

    


