<?php

class Admin_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        session_start();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $checklogin = $_SESSION["login_data"];
        if($checklogin["roles"]==1){
            $this->_redirector->gotoSimple('index', 'index','index');
        }
        if( count($_SESSION["login_data"]) == 0 ){
            $this->_redirector->gotoSimple('index', 'index','index');
        }
        if( count($_SESSION["login_data"]) == 0 ){
            $this->_redirector->gotoSimple('index', 'index');
        }
	

    }

  

    public function indexAction()
    {    
        $this->_redirector = $this->_helper->getHelper('Redirector');   
        $this->_redirector->gotoSimple('index', 'urls','admin');
        $db = Zend_Db_Table::getDefaultAdapter();
        $select_clients = $db->select()->from('users_tbl')->where('roles = ?','1');
	$getData_clients = $db->fetchAll($select_clients);
	//print_r($getData_clients); exit;
	$this->view->data = array('user' => $getData_clients);
       
    }

   
    /* add new client */
     public function addnewclientAction()
    {
        
	$db = Zend_Db_Table::getDefaultAdapter();
	if($this->getRequest()->isPOST()) {
            
	    $email = $db->fetchAll("select * from users_tbl where email=?",array($_POST['email']),2);
	    if(empty($email)){
            if($_POST['password'] == $_POST['confirmpass'])
            {
	    $data= array(
		"first_name"=> $_POST['first_name'],
		"last_name"=> $_POST['last_name'],
		"email"=> $_POST['email'],
		"password"=>md5($_POST['password']),
		"status"=> '1',
		"roles"=> '1',
			 );
	$n= $db->insert('users_tbl',$data);
	if($n){
	    $this->view->msg=array('messg'=>"<div  class='alert alert-success'>Client Created Successfully</div>",'success'=>1);
             
        //    $id=$db->fetchAll("select  id from users_tbl where email=?",array($_POST['email']),2);
        //    if(!file_exists("users")){
        //       mkdir("users", 0777); 
        //    }
        //          
        //    if(file_exists("users")){
        //       mkdir("users/".$id[0]['id'], 0777);
        //       
        //    }
        //$urlOptions = array('module'=>'admin', 'controller'=>'index', 'action'=>'index');
        //$this->_helper->redirector->gotoRoute($urlOptions);  
	}else{
	    $this->view->msg=array("messg" => "<div  class='alert alert-success'>Client Not Created </div>");
	}
	}else{
            $this->view->msg=array("messg" => "<div  class='alert alert-danger'>Password Mismatch</div>");
        }
        }
        else{
	    $this->view->msg=array("messg" => "<div  class='alert alert-danger'>Client already exist with same Email-Id</div>");
	}
	
	
    }}
    
    /* edit client information */
     public function editclientAction()
    {
	$db = Zend_Db_Table::getDefaultAdapter();
	$id=$this->getRequest()->getParam('id');
	if($this->getRequest()->isGET()) {
	    
	    $user=$db->fetchAll("select * from users_tbl where id=?",array($id),2);
	    $this->view->data=array("user"=>$user);
	}
	if($this->getRequest()->isPOST()) {
	$email = $db->fetchAll("select * from users_tbl where email=? and id!=?",array($_POST['email'],$id),2);
	if(empty($email)){
	 if($_POST['password']) {
            if($_POST['password'] == $_POST['confirmpass'])
            {
	    $data= array(
		"first_name"=> $_POST['first_name'],
		"last_name"=> $_POST['last_name'],
		"email"=> $_POST['email'],
		"password"=>md5($_POST['password']),
		"status"=> $_POST['status']
		
			 );
	 }else{
            $this->view->msg="<div  class='alert alert-danger'>Opps! Password Mismatch!!</div>";
	     $user=$db->fetchAll("select * from users_tbl where id=?",array($id),2);
	    $this->view->data=array("user"=>$user);
         }}
         
         else{
	    $data= array(
		"first_name"=> $_POST['first_name'],
		"last_name"=> $_POST['last_name'],
		"email"=> $_POST['email'],
		"status"=> $_POST['status']
		
			 );
	 }
        if(!empty($data)){
	$n= $db->update('users_tbl',$data,'id ='.$id);
         
        if(!empty($n)){
	    $this->view->msg="<div  class='alert alert-success'>Client Information Updated Successfully</div>";
	    $user=$db->fetchAll("select * from users_tbl where id=?",array($id),2);
	    $this->view->data=array("user"=>$user);
	}else{
	     $this->view->msg="<div  class='alert alert-warning'>Opps! Nothing to update !!</div>";
	     $user=$db->fetchAll("select * from users_tbl where id=?",array($id),2);
	    $this->view->data=array("user"=>$user);
	}
	}}else{
	    $this->view->msg="<div  class='alert alert-danger'>Client already exist with same Email-Id</div>";
            $user=$db->fetchAll("select * from users_tbl where id=?",array($id),2);
	    $this->view->data=array("user"=>$user);
	}
        
        }
    
    }
    
    /* for deactivating any client */
    public function deactivateAction()
    {
	$db = Zend_Db_Table::getDefaultAdapter();
	$param=$this->getRequest()->getParam('id');
	$select_clients_id = $db->select()->from('users_tbl')->where('id = ?',$param) ;
        $getData_clients_id = $db->fetchRow($select_clients_id);
	if($getData_clients_id['status'] == 0){
	   $status='1';	    
	}else{
	   $status='0';	 
	}
	 $data=array(
			'status' => $status
			);
	 $db->update('users_tbl',$data,'id = '.$param);
	 echo $status;
	die; 
    }
    
    /* delete client */
    public function deleteclientAction(){

    $db = Zend_Db_Table::getDefaultAdapter();
    $id=$this->getRequest()->getParam('id');
    $n=$db->delete('users_tbl','id='.$id);
    if($n){
	$this->view->msg ='Client deleted successfully !';
        $urlOptions = array('module'=>'admin', 'controller'=>'index', 'action'=>'index');
        $this->_helper->redirector->gotoRoute($urlOptions);
    }else{
	$this->view->msg ='Unable to delete client, kindly retry !';
    }
    }
    public function logoutAction()
	{
          session_destroy();
	  $this->_redirector->gotoSimple('index', 'index','index');
	}
        
        
        
    /* upload csv  action*/
    
    public function uploadedcsvAction(){
    $db = Zend_Db_Table::getDefaultAdapter();
    $id=$this->getRequest()->getParam('id');
    $select_csv_id = $db->select()->from('csv_tbl')->where('client_id = ?',$id) ;
    $getData_csv_id = $db->fetchAll($select_csv_id);
    $this->view->data=array('csv'=>$getData_csv_id,'id'=>$id);
    $request = new Zend_Controller_Request_Http;
    
       if($request->isPost()) {   
        if(@$_POST['Id']){
        /* file upload using zend file tranfer */
        $upload = new Zend_File_Transfer();
        $files = $upload->getFileInfo();// Returns all known internal file information
        $newname=date('Y-d-m h:i:s').'_'.$files['uploadedfile']['name'];
        $upload->addFilter('Rename', $newname);//filter to rename the file 
        $upload->addValidator('Extension', false, 'csv');//validation for csv file
        //print_r($files);die;
        if ($upload->isValid($files['uploadedfile']['name'])) {
        //$adapter = new Zend_File_Transfer_Adapter_Http();
        $upload->setDestination('users/'.$_POST['Id']);
        if (!$upload->receive()) {
            $messages = $adapter->getMessages();
            echo implode("\n", $messages);
        
    }
    /* insert file name to dtabase */
    $data= array(
		"client_id"=> $_POST['Id'],
		"filename"=> $newname,
		"created_on"=> date('Y-m-d')
			 );
	$n= $db->insert('csv_tbl',$data);
        $_POST = array();
        $_POST['Id']='';
        $select_csv_id = $db->select()->from('csv_tbl')->where('client_id = ?',$id) ;
        $getData_csv_id = $db->fetchAll($select_csv_id);
        $this->view->data=array('csv'=>$getData_csv_id,'id'=>$id);
        $urlOptions = array('module'=>'admin', 'controller'=>'index', 'action'=>'uploadedcsv','id'=>$id);
        $this->_helper->redirector->gotoRoute($urlOptions);
        
    }else{
      $this->view->msg="<alert class='alert alert-danger col-md-12 text-center'>File formate Incorrect</alert>";  
    }
        }}
    
    }
     
     
        
    public function uploadcsvAction(){
        
    $valid_formats = array("csv");
    function getExtension($str) 
    {
         $i = strrpos($str,".");
         if (!$i) { return ""; } 

         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
    }
        
    $db = Zend_Db_Table::getDefaultAdapter();
    $id=$this->getRequest()->getParam('id');
    $select_csv_id = $db->select()->from('csv_tbl')->where('client_id = ?',$id) ;
    $getData_csv_id = $db->fetchAll($select_csv_id);
    $this->view->data=array('csv'=>$getData_csv_id,'id'=>$id);
    $request = new Zend_Controller_Request_Http;
    
       if($request->isPost()) {   
        if(@$_POST['Id']){
           // print_r($_POST);echo '<br>';
             // print_r($_FILES ['uploadedfile']['name']);die;
        
        /* file upload using zend_amazon_s3 file tranfer */
        $filename=$_FILES ['uploadedfile']['name'];
        $fileTempName = $_FILES['uploadedfile']['tmp_name'];
        $newname="CSV-".date('Y-d-mh:i:s').".csv";
        $ext = getExtension($filename);
        
        if(strlen($filename) > 0)
        {
        if(in_array($ext,$valid_formats))
        {
        //include the S3 class             
        if (!class_exists('S3'))require_once('S3.php');
        //AWS access info
        if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJNN5ADRENASOYTCQ');
        if (!defined('awsSecretKey')) define('awsSecretKey', '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');
        //instantiate the class
        $s3 = new S3(awsAccessKey, awsSecretKey);
        //create a new bucket
        $s3->putBucket("data-capture-csv",S3::ACL_PUBLIC_READ);
 
        //move the file
        if ($s3->putObjectFile($fileTempName, "data-capture-csv",$_POST['Id'].'/'.$newname, S3::ACL_PUBLIC_READ)) {
        echo "We successfully uploaded your file.";
        
        
        /* insert file name to dtabase */
        $data= array(
		"client_id"=> $_POST['Id'],
		"filename"=> $newname,
		"created_on"=> date('Y-m-d')
			 );
	$n= $db->insert('csv_tbl',$data);
        $_POST = array();
        $_POST['Id']='';
        $select_csv_id = $db->select()->from('csv_tbl')->where('client_id = ?',$id) ;
        $getData_csv_id = $db->fetchAll($select_csv_id);
        $this->view->data=array('csv'=>$getData_csv_id,'id'=>$id);
        $urlOptions = array('module'=>'admin', 'controller'=>'index', 'action'=>'uploadcsv','id'=>$id);
        $this->_helper->redirector->gotoRoute($urlOptions);
        }else{
        echo "Something went wrong while uploading your file... sorry.";
        }
        
        }else
        $this->view->msg = "<alert class='alert alert-danger'>Invalid file, please upload csv file.</alert>";

        }
        else
        $this->view->msg = "Please select csv file.";

    }
    }

    }       
    /* delete csv file */
    
     public function deletecsvAction(){
       $db = Zend_Db_Table::getDefaultAdapter();
       $id=$this->getRequest()->getParam('id');
       $client_id=$db->fetchAll('select * from csv_tbl where id=?',array($id),2);
       $c_id=$client_id[0]['client_id'];
       $n=$db->delete('csv_tbl','id='.$id);
       if (!class_exists('S3'))require_once('S3.php');
        //AWS access info
        if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJNN5ADRENASOYTCQ');
        if (!defined('awsSecretKey')) define('awsSecretKey', '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');
        //instantiate the class
        $s3 = new S3(awsAccessKey, awsSecretKey);
        $result = $s3->deleteObject('data-capture-csv',$client_id[0]['client_id'].'/'.$client_id[0]['filename']);   
    if($n){
	$this->view->msg ='CSV file deleted successfully !';
        $urlOptions = array('module'=>'admin', 'controller'=>'index', 'action'=>'uploadcsv','id'=>$c_id);
        $this->_helper->redirector->gotoRoute($urlOptions);
    }else{
	$this->view->msg ='Unable to delete CSV, kindly retry !';
    } 
     }
     /* delete csv file */
    
   
}

	


