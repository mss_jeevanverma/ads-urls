
<?php

class Admin_CsvController extends Zend_Controller_Action
{

    public function init()
    {
               /* Initialize action controller here */
        session_start();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $checklogin = $_SESSION["login_data"];
        if($checklogin["roles"]==1){
            $this->_redirector->gotoSimple('index', 'index','index');
        }
        if( count($_SESSION["login_data"]) == 0 ){
            $this->_redirector->gotoSimple('index', 'index','index');
        }
      

    }


 public function indexAction()
    {
        unset($_SESSION["filter_data"]);
        
        $db = Zend_Db_Table::getDefaultAdapter();
        $conn = new Mongo('localhost');
	MongoCursor::$timeout = -1;
        $mongodb = $conn->users;
        //$collection = $mongodb->application;
        //$getData_apps = $collection->distinct("app_name");
	$collection_count = $mongodb->device_unique;	
	$total_device_count=$collection_count->find()->count();
	$total_device_count_us=$collection_count->find(array('country_code'=>'United States'))->count();		
	$collection = $mongodb->device_statistics;
    	$data_device_per_day=$collection->find();
	$collection_us = $mongodb->unique_user_us;
    	$data_device_per_day_us=$collection_us->find();
        $getData_countries = $db->fetchAll("SELECT * FROM countries_tbl", 2);
	$select_csv_status = $db->fetchAll("select status from csv_status_tbl where id=1",2);
	$select_filtered_data = $db->fetchAll("select * from filter_csv_data",2);
	$request = new Zend_Controller_Request_Http;
         
            if($request->isPost()){
                $params = $request->getParams();
                print_r($params);print_r($_POST); exit;
            }
        $this->view->data = array(
            "countries"=>$getData_countries,
            //"apps"=> $getData_apps,
	    "status"=>$select_csv_status[0]['status'],
	    "data_device_per_day"=>$data_device_per_day,
	    "data_device_per_day_us"=>$data_device_per_day_us,
	    "total_device_count"=>$total_device_count,
	    "total_device_count_us"=>$total_device_count_us,
	    "filter_data"=>$select_filtered_data
            );
    }

 public function devicecountAction()
    {
	 $conn = new Mongo('localhost');
	MongoCursor::$timeout = -1;
        $mongodb = $conn->users;
        //$collection = $mongodb->application;
        //$getData_apps = $collection->distinct("app_name");
	$collection_count = $mongodb->device_unique;	
	$total_device_count=$collection_count->find()->count();
	$total_device_count_us=$collection_count->find(array('country_code'=>'United States'))->count();		
	$collection = $mongodb->device_statistics;
    	$data_device_per_day=$collection->find()->sort(array('from' => -1));
	$collection_us = $mongodb->unique_user_us;
    	$data_device_per_day_us=$collection_us->find()->sort(array('from' => -1));
	$this->view->data = array(
	    "data_device_per_day"=>$data_device_per_day,
	    "data_device_per_day_us"=>$data_device_per_day_us,
	    "total_device_count"=>$total_device_count,
	    "total_device_count_us"=>$total_device_count_us
            );

}
 public function apprecordsAction (){
 
    $request = new Zend_Controller_Request_Http;
    $conn = new Mongo('localhost');
    $mongodb = $conn->users;
    $collection = $mongodb->application;
    
        try{
               
            if(!isset($_SESSION["filter_data"])){
		$params = $request->getParams(); 
                $_SESSION["filter_data"] = $params;
                
                if(strlen($params["date_from"])){
                    $_SESSION["filter_data"]["date_from"] = $params["date_from"]." 00:00:00";
                    $_SESSION["filter_data"]["date_to"] = $params["date_to"]." 00:00:00";
                }
            }
	   
	    $database = Zend_Db_Table::getDefaultAdapter();
	    $select_csv_status=$database->fetchAll("select status from csv_status_tbl where id=1");
	    if($select_csv_status[0]['status'] == 'complete'){
	    $data1=array(
			status => 'pending' 
		);
	    $set_csv_status = $database->update('csv_status_tbl', $data1, 'id = 1');
	    unlink('csv/filter.json');
	    //$files = glob('csv/*'); // get all file names
	    //foreach($files as $file){ // iterate files
	    //if(is_file($file))
	    //unlink($file); // delete file
	    //}
	    
            //print_r($_SESSION["filter_data"]);
	    
	    
	    /* SAVE THE FILER DATA TO DATABASE */
	    $p=$_SESSION["filter_data"];
	    $filter_data=array(
		'filter_data'=>json_encode($p)
	    );
	    $filter_csv_data=$database->insert("filter_csv_data",$filter_data);
	    
	    /* SAVE THE FILER DATA TO FILE */
	    file_put_contents('csv/filter.json', json_encode($p));
	    
	   $cmd = "php /var/www/html/data-capture/public/csv_creator.php"; // Start the process and continue outputting your response to the user  
    exec($cmd . " > /dev/null &"); 
	    }else{
               $this->view->msg = 'Already running';
		}

	    /*
           // $this->csvgenrateAction(); die("after csv");
            $csv_data = $this->getfiltereddata($_SESSION["filter_data"]); 
	
               // $_SESSION["filter_data"] = $params;
              if($csv_data){
                foreach($csv_data as $data){
                        $result[] = $data; 
                }
            //echo "<pre>"; print_r($result); die;
	  
            Zend_View_Helper_PaginationControl::setDefaultViewPartial('controls.phtml');
            $paginator = Zend_Paginator::factory($csv_data);
            //$paginator->setCurrentPageNumber($this->_getParam('page', 1));
            
            $page = $this->_request->getParam('page');
            if (empty($page)) { $page = 1; }
            $paginator->setCurrentPageNumber($page);
           
            $itemperpage=10;
            $paginator->setItemCountPerPage($itemperpage);
             if( $page == 1){
                $itemcount=0;
            }else{
                $itemcount=$itemperpage*($page-1);
            }
            $this->view->data = array("apps_data"=> $paginator,"Itemcount"=>$itemcount,"count"=>count($result));
            }
            */
	    
	    
	    
               
        }catch(Exception $e){
           //echo "<pre>"; print_r($e); die;
        }
    }


public function csvgenrateAction(){
    try{
    $filter_data = $_SESSION["filter_data"];
	$conn = new Mongo('localhost');
	$mongodb = $conn->users;
    $date_from =  new MongoDate(strtotime($filter_data["date_from"]));
    MongoCursor::$timeout = -1;
    $date = new DateTime($filter_data["date_to"]);
    $date->add(new DateInterval('P1D'));
    $date_to_new = $date->format('Y-m-d H:i:s');
    $date_to =  new MongoDate(strtotime($date_to_new));

	if($filter_data["selectallapps"]){
	    $collection = $mongodb->device;
	    $count=$collection->find(
                     array('$and' => 
                            array(
                                    array("country_code" => array('$in' => $filter_data["country"])),
                                    array("device" => array('$in' => $filter_data["platform"])),
                                        (
                                            isset($filter_data["date_from"])
                                            ?array("updated_at" =>array('$gt' => $date_from, '$lte' => $date_to)                                  )
                                            :array("updated_at" =>array())
                                        ),
                                    
                                )
                        )  
                    )->timeout(-1)->count();
	}else{
	    $collection = $mongodb->application;
	    $count=$collection->find(
                     array('$and' => 
                            array(
                                   array("app_name" => array('$in' => $filter_data["app_name"])),
                                    array("country_code" => array('$in' => $filter_data["country"])),
                                    array("device" => array('$in' => $filter_data["platform"])),
                                        (
                                            isset($filter_data["date_from"])
                                            ?array("updated_at" =>array('$gt' => $date_from, '$lte' => $date_to)                                  )
                                            :array("updated_at" =>array())
                                        ),
                                )
                        )  
                    )->timeout(-1)->count();
	   
	}


	$no_of_records=30000;
	$no_of_csv=$count/$no_of_records;
	$no_of_csv= ceil($no_of_csv);
	//echo $no_of_csv; die("no of loop");
    	for($i=0;$i<$no_of_csv;$i++){
    	    echo $i;
            echo "<hr>";
    	    $csv_data = $this->getfiltereddata($filter_data,$i*$no_of_records,$no_of_records);
           
    	    $csvname='/'.date("m/d/Y")."csv";
    	    
    	    //header('Content-Type: application/csv; charset=utf-8');
    	    //header('Content-Disposition: attachment; filename=app_records.csv');                    
    	    //header("Content-Transfer-Encoding: UTF-8");
    	    $output = @fopen("csv/app_records_".$i.".csv", 'w');
    	    
    		  
    	    if($filter_data["selectallapps"]){
    	     //if(true){
        		$heading_arr = array('Device ID' , "OS Version", "Platform", "Country", "Device Type", "Device Maker", "Device Model");
        		for($j=1;$j<=400;$j++){
        		    $heading_arr[] = "App_".$j;
        		}
        		fputcsv($output, $heading_arr);
        		foreach($csv_data as $data){

        			$main_arr = array($data["device_id"],$data["os_ver"],$data["device"],$data["country_code"],$data["device_type"],$data["maker"],$data["model"]);
        			$apps = explode(", ",$data["app_data"]);
        			foreach($apps as $app){
        			    $main_arr[]=$app;
        			} 
        			fputcsv($output,$main_arr);  
        		    }
    		 
    	    }else{
        		fputcsv($output, array('Device ID', 'Application Name' , "OS Version", "Platform", "Country", "Device Type", "Device Maker", "Device Model",  "Date Installed" ));
        		foreach($csv_data as $data){
        			$main_arr = array($data["device_id"],$data["app_name"],$data["os_ver"],$data["device"],$data["country_code"],$data["device_type"],$data["maker"],$data["model"],$data["date_time_installed"]);
        			fputcsv($output,$main_arr );  
        		    }
    	    }
        
    		fclose($output);
            //echo "<pre>"; print_r($csv_data); 
    	    //$this->_redirector->gotoSimple('apprecords', 'csv','admin');
    	 
    	}
        }catch(Exception $e){
           echo "<pre>"; print_r($e); die;
        }
    }

 public function mypaginationcontrolAction (){
    
 }

public function getfiltereddata($data,$skip,$limit){
    
    $conn = new Mongo('localhost');
    $mongodb = $conn->users;
    
    try{
        if($data){  
            $params = $data;
            $date_from =  new MongoDate(strtotime($params["date_from"]));
            
    		$date = new DateTime($params["date_to"]);
    		$date->add(new DateInterval('P1D'));
    		$date_to_new = $date->format('Y-m-d H:i:s');
	        $date_to =  new MongoDate(strtotime($date_to_new));
		//echo date('Y-m-d H:i:s', strtotime('+1 day', '2015-04-15 00:00:00'));
           if($data["selectallapps"]){
		//if(true){
                unset($data["app_name"]);
                $collection = $mongodb->device;
                //$collection = $mongodb->device_unique;
                $csv_data = $collection->find(
                     array('$and' => 
                            array(
                                    array(
                                            "country_code" => array('$in' => $params["country"])
                                        ),
                                    array(
                                            "device" => array('$in' => $params["platform"])
                                        ),
                                        (
                                            isset($params["date_from"])
                                            ?array("updated_at" =>array('$gt' => $date_from, '$lte' => $date_to)                                  )
                                            :array("updated_at" =>array())
                                        ),
                                    
                                )
                        )  
                    )->skip($skip)->limit($limit)->timeout(-1);
		      
                    foreach($csv_data as $data){

                        foreach($data["app_data"] as $apps){
                                $app_names[] =  $apps["app_name"];
                        }

                        $data["app_data"]= implode(", ",$app_names);
                        $result[] = $data;
                        unset($app_names);
                    }
            }else{
		
                $collection = $mongodb->application;

                $csv_data = $collection->find(
                     array('$and' => 
                            array(
                                   array(
                                            "app_name" => array('$in' => $params["app_name"])                       
                                        ),
                                    array(
                                            "country_code" => array('$in' => $params["country"])
                                        ),
                                    array(
                                            "device" => array('$in' => $params["platform"])
                                        ),
                                        (
                                            isset($params["date_from"])
                                            ?array("updated_at" =>array('$gt' => $date_from, '$lte' => $date_to)                                  )
                                            :array("updated_at" =>array())
                                        ),
                                )
                        )  
                    )->skip($skip)->limit($limit)->timeout(-1);
                foreach($csv_data as $data){

                       $result[] = $data; 
                    }

            } 
                    //)->limit(1000);
                    return @$result;
                  
            }
        }catch(Exception $e){
           echo "<pre>"; print_r($e); die;
        }

}

/*
    public function csvgenrateAction(){


        $results = json_decode($_POST["data"],true);
        foreach ($results as $key =>  $result){
            $platform = $result["device"];
            if($platform==0)
                { $result["device"]="Android"; }
            else
                { $result["device"]="IPhone"; }
            $csv_data[]= $result;
        }
        //echo "<pre>"; print_r($csv_data); 
        header('Content-Type: application/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=validcepsapp.csv');                    
        header("Content-Transfer-Encoding: UTF-8");
        // create a file pointer connected to the output stream
        $output = @fopen('php://output', 'w');                     
            // output the column headings                    
        fputcsv($output, array('Device ID', 'Application Name' , "Platform (OS)", "Country Name", "Device Free Memory", "Device Used Memory", "Device Total Memory", "Device wifi data Sent", "Device Wifi Data Recieved", "Device WWAN Data Sent", "Device WWAN Data Recieved", "Data Update Date" ));
        foreach($csv_data as $data){
                fputcsv($output, $data);  
            }
        
        fclose($output);
        exit;
    }

*/

   /* public function indexAction()
    {
        // action body
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $select_countries = $db->select()->from('countries_tbl');
        $getData_countries = $db->fetchAll($select_countries);

        //$select_segments = $db->select()->from('category_tbl');
        //$getData_segments = $db->fetchAll($select_segments);
        $getData_apps = $db->fetchAll("SELECT DISTINCT(app_name) FROM app_tbl", 2);

       // $getData_os_ver = $db->fetchAll("SELECT DISTINCT(os_ver) FROM device_tbl", 2);
        //$getData_model = $db->fetchAll("SELECT DISTINCT(model) FROM device_tbl", 2);
        //$getData_device_type = $db->fetchAll("SELECT DISTINCT(device_type) FROM device_tbl", 2);
        //echo "<pre>"; print_r($select_apps); die;
        //$select_apps = $db->select("distinct(app_name)")->from('app_tbl');
        //$getData_apps = $db->fetchAll($select_apps);

        $request = new Zend_Controller_Request_Http;
    
    
            if($request->isPost()){
                    $params = $request->getParams();
                    print_r($params);print_r($_POST); exit;
            }
        //print_r($getData_countries);die;
        $this->view->data = array(
            "countries"=>$getData_countries,
            //"segments"=>$getData_segments,
            "apps"=> $getData_apps,
            //"os_ver"=> $getData_os_ver,
            //"model"=> $getData_model,
            //"device_type"=> $getData_device_type
            );
    }*/
/*
    public function apprecordsAction (){

        $request = new Zend_Controller_Request_Http;
        $db = Zend_Db_Table::getDefaultAdapter();
        try{
            if($request->isPost()){
                $params = $request->getParams();
                //$segment = $params["segment"];
                $date_from = $params["date_from"];
                $date_to = $params["date_to"];
                $app_name = $params["app_name"];
                $platform = $params["platform"];
                $country = $params["country"];
                $query = "SELECT 
                device_tbl.device_id,app_tbl.app_name,device_tbl.device,countries_tbl.country_name,device_tbl.free_memory,
                device_tbl.used_memory ,device_tbl.total_memory,device_tbl.wifi_sent,device_tbl.wifi_recieved,device_tbl.wwan_sent,
                device_tbl.wwan_recieved,app_tbl.date_created                             
                        FROM device_tbl,app_tbl,countries_tbl 
                        WHERE device_tbl.id = app_tbl.device_id 
                        AND countries_tbl.country_code = device_tbl.country_code 
                        AND countries_tbl.country_code ='$country'
                        AND app_tbl.app_name = '$app_name' 
                        AND device_tbl.date_created >= '$date_from' 
                        AND device_tbl.date_created <= '$date_to' 
                        AND device_tbl.device = $platform";
                $result = $db->fetchAll($query, 2);
               // echo "<pre>"; print_r($result); echo "</pre>";
                $this->view->data = array("apps_data"=> $result);
                
                    //$result = $db->fetchAll($query, array($email,$password), 2);
                    
            }

        }catch(Exception $e){
           echo "<pre>"; print_r($e); die;
        }
    }
*/



 // sunny@ application name autocomplete
    
    public function applicationdataAction(){
        try{
            $params = Zend_Controller_Front::getInstance()->getRequest()->getParams();
            $app_nam = $params['app_nam'];
            if($app_nam){
                $conn = new Mongo('localhost');
                $mongodb = $conn->users;
                $collection = $mongodb->unique_apps;
                $getData_apps = $collection->distinct("app_name",array('app_name'=>array('$regex' => new MongoRegex("/^$app_nam/i"))));
                print_r(json_encode($getData_apps,JSON_UNESCAPED_SLASHES));
                die();
                
            }else{ die(); }
        }catch(Exception $e){
            print_r(json_encode($e->getMessage()));die();
        }
    }
    
    /************************* Filter data execute function ******************/
    
    public function filtereddataAction(){
	$filter_id=$_POST['id'];     
        $db = Zend_Db_Table::getDefaultAdapter();
	$select_filtered_data1 = $db->fetchAll("select * from filter_csv_data  where id=?",array($filter_id),2);
	//print_r($select_filtered_data1[0]['filter_data']);
	$select_csv_status=$db->fetchAll("select status from csv_status_tbl where id=1");
	    if($select_csv_status[0]['status'] == 'complete'){
	    unlink('csv/filter.json');	    	    
		
	    $data1=array(
			status => 'pending' 
		);
	    $set_csv_status = $db->update('csv_status_tbl', $data1, 'id = 1');
	    file_put_contents('csv/filter.json', $select_filtered_data1[0]['filter_data']);
	    
	   $cmd = "php /var/www/html/data-capture/public/csv_creator.php"; // Start the process and continue outputting your response to the user  
	   exec($cmd . " > /dev/null &");
	//$this->view->data = array('msg'=>"csv is being generated please check the server.");
	    }else{
	    //$this->view->data = array('msg'=>"old csv already being generated");
	    echo 'running';
	      	} 
	    die;
    }
    
    /************************* Filter data edit function ******************/
    
   public function editfilterAction(){
    $filter_id = $this->getRequest()->getParam('id');
    $db = Zend_Db_Table::getDefaultAdapter();
    $getData_countries = $db->fetchAll("SELECT * FROM countries_tbl", 2);
    $select_csv_status = $db->fetchAll("select status from csv_status_tbl where id=1",2);
    $select_filtered_data = $db->fetchAll("select * from filter_csv_data where id = ?",array($filter_id),2);
    $this->view->data = array(
            "countries"=>$getData_countries,
            //"apps"=> $getData_apps,
	    "status"=>$select_csv_status[0]['status'],
	    "filterdata"=>$select_filtered_data
   );
    //echo $request;die;
   }
   
    /************************* Filter data delete function ******************/
    
    public function deletefiltereddataAction(){
	$filter_id = $_POST['id'];
	//echo $filter_id;die; 
        $db = Zend_Db_Table::getDefaultAdapter();
	$remove_recently_filtered= $db->delete('filter_csv_data', 'id = '.$filter_id.'');
	die;
    }

}

