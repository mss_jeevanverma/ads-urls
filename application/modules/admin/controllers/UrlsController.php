<?php

class Admin_UrlsController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        session_start();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $checklogin = $_SESSION["login_data"];
        if($checklogin["roles"]==1){
            $this->_redirector->gotoSimple('index', 'index','index');
        }
        if( count($_SESSION["login_data"]) == 0 ){
            $this->_redirector->gotoSimple('index', 'index','index');
        }
        if( count($_SESSION["login_data"]) == 0 ){
            $this->_redirector->gotoSimple('index', 'index');
        }
    

    }

  

    public function indexAction()
    {       
        $db = Zend_Db_Table::getDefaultAdapter();

       $getData_urls = $db->fetchAll("SELECT * FROM urls_tbl ORDER BY id DESC",array(),2);    

       $this->view->data = array('urls' => $getData_urls);
       
    }

   
    /* add new Set */
     public function addnewurlAction()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $countries = $db->fetchAll("select * from countries_tbl",array(),2);
        
        if($this->getRequest()->isPOST()) {

            $n= $db->insert('urls_tbl',$_POST);

            if($n){
                $this->view->msg=array('messg'=>"<div  class='alert alert-success'>URL Created Successfully</div>",'success'=>1);
                $this->_redirector = $this->_helper->getHelper('Redirector');   
                $this->_redirector->gotoSimple('index', 'urls','admin');
            }else{
                $this->view->msg=array("messg" => "<div  class='alert alert-success'>URL Not Created </div>");
            }

        }
        $_POST = '';
        $this->view->data=array("countries" => $countries);
        
    }
    
    /* edit set information */
     public function editurlAction()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $id=$this->getRequest()->getParam('id');
        $countries = $db->fetchAll("select * from countries_tbl",array(),2);
        if($this->getRequest()->isGET()) {
            
            $url=$db->fetchAll("select * from urls_tbl where id=?",array($id),2);

        }

        if($this->getRequest()->isPOST()) {

            $n= $db->update('urls_tbl',$_POST,'id ='.$id);

            $url=$db->fetchAll("select * from urls_tbl where id=?",array($id),2);

            if($n){
                $this->view->msg="<div  class='alert alert-success'>URL Information Updated Successfully</div>";
            }else{
                $this->view->msg="<div  class='alert alert-success'>Somthing Went Wrong , Try Again.</div>";
            }
           
        }
            $this->view->data=array("url"=>$url,"countries" => $countries);    
    }

    
    /* delete set */
    public function deleteurlAction(){

        $db = Zend_Db_Table::getDefaultAdapter();
        $id=$this->getRequest()->getParam('id');

        $n=$db->delete('urls_tbl','id='.$id);

        if($n){
            $this->view->msg ='Url deleted successfully !';
            $urlOptions = array('module'=>'admin', 'controller'=>'urls', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }else{

           $this->view->msg ='Unable to delete Url, kindly retry !';
        }
    }

    public function logoutAction()
    {
          session_destroy();
      $this->_redirector->gotoSimple('index', 'index','index');
    }
        

}

    


