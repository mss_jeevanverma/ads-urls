<?php

class Admin_SegmentsController extends Zend_Controller_Action
{

    public function init()
    {
            /* Initialize action controller here */
        session_start();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $checklogin = $_SESSION["login_data"];
        if($checklogin["roles"]==1){
            $this->_redirector->gotoSimple('index', 'index','index');
        }
        if( count($_SESSION["login_data"]) == 0 ){
            $this->_redirector->gotoSimple('index', 'index','index');
        }

    }

    public function indexAction()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select_segments = $db->select()->from('category_tbl');
	$getData_segments = $db->fetchAll($select_segments);
	//print_r($getData_clients); exit;
	$this->view->data = array('segment' => $getData_segments);
    }
    
    /* add segment start */
      public function addnewsegmentAction()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        
	if($this->getRequest()->isPOST()) {
        $segment = $db->fetchAll("select * from category_tbl where category_name=?",array($_POST['segment_name']),2);
        if(empty($segment)){

        $data= array(
		"category_name"=> $_POST['segment_name']
            
        );
        $n= $db->insert('category_tbl',$data);
        if($n){
        $this->view->msg="<div  class='alert alert-success'>Segment Added successfully!!</div>";

        }else{
        $this->view->msg="<div  class='alert alert-warning'>Opps unable to add!!</div>";
        
        }
    }else{
        $this->view->msg="<div  class='alert alert-warning'>Opps segment already exist!!</div>";
    }
    }

}
    /* add segment end */
    
     /* add segment start */
      public function editsegmentAction()
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $id=$this->getRequest()->getParam('id');
        $segment=$db->fetchAll("select * from category_tbl where id=?",array($id),2); 
	$this->view->data=array("segment"=>$segment);
	if($this->getRequest()->isPOST()) {
        $data= array(
		"category_name"=> $_POST['segment_name']
            
        );
        $n= $db->update('category_tbl',$data,'id='.$id);
        if($n){
        $this->view->msg="<div  class='alert alert-success'>Segment Updated successfully!!</div>";
        $segment=$db->fetchAll("select * from category_tbl where id=?",array($id),2); 
	$this->view->data=array("segment"=>$segment);
        }else{
        $this->view->msg="<div  class='alert alert-warning'>Nothing to update!!</div>";
        $segment=$db->fetchAll("select * from category_tbl where id=?",array($id),2); 
	$this->view->data=array("segment"=>$segment);    
        }
    }


}
    /* add segment end */
    
     /* delete segment start */
     public function deletesegmentAction(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $id=$this->getRequest()->getParam('id');   
         $n=$db->delete('category_tbl','id='.$id);
    if($n){
	$this->view->msg ="<div  class='alert alert-success'>Segment deleted successfully !</div>";
        $urlOptions = array('module'=>'admin', 'controller'=>'segments', 'action'=>'index');
        $this->_helper->redirector->gotoRoute($urlOptions);
    }else{
        $this->view->msg ="<div  class='alert alert-warning'>Unable to delete client, kindly retry !</div>";
    }    
        
     }
     /* delete segment end */
}

