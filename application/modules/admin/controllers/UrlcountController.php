<?php

class Admin_UrlcountController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        session_start();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $checklogin = $_SESSION["login_data"];
        if($checklogin["roles"]==1){
            $this->_redirector->gotoSimple('index', 'index','index');
        }
        if( count($_SESSION["login_data"]) == 0 ){
            $this->_redirector->gotoSimple('index', 'index','index');
        }
        if( count($_SESSION["login_data"]) == 0 ){
            $this->_redirector->gotoSimple('index', 'index');
        }
	

    }

  

    public function indexAction()
    {       
       $db = Zend_Db_Table::getDefaultAdapter();
       $getData_urls = $db->fetchAll("SELECT * FROM urls_tbl ORDER BY id DESC",array(),2);
       $this->view->data = array('urlcount' => $getData_urls);
    }
}

   ?>