<?php

class Client_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        session_start();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $checklogin = $_SESSION["login_data"];
        if( count($_SESSION["login_data"]) == 0 ){
            $this->_redirector->gotoSimple('index', 'index' ,'indexAction');
        }
    }

    public function indexAction()
    {
        //print_r($_SESSION["login_data"]['email']);die;
        $db = Zend_Db_Table::getDefaultAdapter();
        $email=$_SESSION["login_data"]['email'];
    
        $select_clients = $db->select()->from('users_tbl')->where('roles = ?','1')->where('email=?',$_SESSION["login_data"]['email']);
	$getData_clients = $db->fetchRow($select_clients);
        if(empty($getData_clients)){
            $urlOptions = array('module'=>'admin', 'controller'=>'index', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);    
        }
        if($getData_clients['status'] == '0'){
        $urlOptions = array('module'=>'admin', 'controller'=>'index', 'action'=>'index');
        $this->_helper->redirector->gotoRoute($urlOptions);    
        }
	//print_r($getData_clients); exit;
	$this->view->data = array('client' => $getData_clients);
        
        
        
        /* post data for update */
        
        if($this->getRequest()->isPOST()) {
            //print_r($_POST);die;
            
            $data=array(
                'first_name' => $_POST['first_name'],
                'last_name' => $_POST['last_name']
                
                        );
            $n=$db->update('users_tbl',$data,"email = '".$email."'");
            if($n){
            $this->view->msg='<alert class="alert alert-success col-md-12">Updated Successfully</alert>';    
            }else{
            $this->view->msg='<alert class="alert alert-warning col-md-12">Opps Nothing to update!!</alert>';      
            }
            
            $select_clients = $db->select()->from('users_tbl')->where('roles = ?','1')->where('email=?',$_SESSION["login_data"]['email']);
            $getData_clients = $db->fetchRow($select_clients);
            //print_r($getData_clients); exit;
            $this->view->data = array('client' => $getData_clients);
            
            
        }
        
    }


}

