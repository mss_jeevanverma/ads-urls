<?php

class Client_ReportController extends Zend_Controller_Action
{

    public function init()
    {
       session_start();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $checklogin = $_SESSION["login_data"];
        if( count($_SESSION["login_data"]) == 0 ){
            $this->_redirector->gotoSimple('index', 'index' ,'indexAction');
        }
    }

    public function indexAction()
    {
      $db = Zend_Db_Table::getDefaultAdapter();
        $email=$_SESSION["login_data"]['email'];
        $select_clients = $db->select()->from('users_tbl')->where('roles = ?','1')->where('email=?',$_SESSION["login_data"]['email']);
	$getData_clients = $db->fetchRow($select_clients);
        if(empty($getData_clients)){
            $urlOptions = array('module'=>'admin', 'controller'=>'index', 'action'=>'index');
        $this->_helper->redirector->gotoRoute($urlOptions);    
        }
        if($getData_clients['status'] == '0'){
        $urlOptions = array('module'=>'admin', 'controller'=>'index', 'action'=>'index');
        $this->_helper->redirector->gotoRoute($urlOptions);    
        }
	//print_r($getData_clients['id']); exit;
        $select_csv=$db->select()->from('csv_tbl')->where('client_id=?',$getData_clients['id']);
	$getData_csv = $db->fetchAll($select_csv);
        $this->view->data = array('csv' => $getData_csv);
    }


}

